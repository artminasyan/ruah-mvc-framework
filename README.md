# Ruah MVC
Welcome to Ruah PHP! Ruah is a light-weight MVC framework focused on ease of use and ability to extend. We aim to speed up the process of building a PHP web application. It comes with bootstrap preinstalled and is very easy to extend and use.

# Folder Structure Conventions

> Folder structure options and naming conventions for Ruah MVC

### Directory layout

    .
    ├── app                     # Website controllers, models, views, libraries
    ├── config                  # MVC configuration files
    ├── core                    # MVC core
    ├── front-resources         # Front-End resources
    ├── .htaccess               # .htaccess server configuration
    ├── index.php               # Website general configuration
    ├── LICENSE                 # MVC LICENSE
    └── README.md               # MVC README.md
    
## License

The Ruah MVC framework is open-source software licensed under the MIT license.